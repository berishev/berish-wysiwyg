import EditorBlock from 'draft-js/lib/DraftEditorBlock.react';
import BlockWrapper from './lib/blockWrapper';
import Editor from './lib/editor';
export { EditorBlock, BlockWrapper };
export default Editor;
